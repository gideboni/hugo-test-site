FROM ubuntu:latest

# Set up
RUN apt update
RUN apt upgrade -y
RUN apt install -y git

# Hugo installation
RUN apt install -y hugo 

# Repo download
RUN git clone https://gitlab.cern.ch/gideboni/hugo-test-site.git

# Page startup
WORKDIR hugo-test-site
CMD hugo server --bind 0.0.0.0 -D
#ENTRYPOINT 
#CMD ["/bin/sh"]